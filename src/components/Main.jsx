import React from "react";
import darkLayer from "../assets/react-layer.svg"
import lightLayer from "../assets/react-layer-light.svg"

export default function Main(props) {
    return (
        <div className={props.darkMode ? "dark main" : "main"}>
            <div className="main-wrapper ">
                <h1>Fun facts about React</h1>
                <img src={props.darkMode ? darkLayer : lightLayer} alt="" className="main-layer" />
                <ul>
                    <li>Was first released in 2013</li>
                    <li>Was originally created by Jordan Walke</li>
                    <li>Has well over 100K stars on GitHub</li>
                    <li>Is maintained by Facebook</li>
                    <li>Powers thousands of enterprise apps, including mobile apps</li>
                </ul>
            </div>
        </div>
    )
}