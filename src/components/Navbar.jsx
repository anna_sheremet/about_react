import React from "react";
import logo from "../assets/react.svg"
export default function Navbar(props) {
    return (
        <div className={props.darkMode ? "dark nav" : "nav"}>
            <div className="nav-image-keeper">
                <img src={logo}  rel="icon" type="image/svg+xml"  alt="img"/>
                <h2>ReactFacts</h2>
            </div>
            <div className="nav-toggler">
                <div className="toggler">
                    <p className="toggler-light">Light</p>
                    <div
                        className="toggler-slider"
                        onClick={props.toggleDarkMode}
                    >
                        <div className="toggler-slider-circle"></div>
                    </div>
                    <p className="toggler-dark">Dark</p>
                </div>
            </div>
        </div>
    )
}